//
// JSON classes
//
// ToDo:
//   - string parsing, escaping and stuff
//
// $Id$
//

unit JSON;

interface

uses
  Classes, // for TStringList
  Contnrs; // for TObjectList

const
  csJSONEscapeCharacter = '\';
  coJSONStringDelimiter = '"';
  coJSONMemberNameValueSeparator = ':';
  coJSONBeginObjectCharacter = '{';
  coJSONEndObjectCharacter = '}';
  coJSONBeginArrayCharacter = '[';
  coJSONEndArrayCharacter = ']';
  coJSONMemberSeparator = ',';
  coJSONReservedWordTrue = 'true';
  coJSONReservedWordFalse = 'false';
  coJSONReservedWordNull = 'null';

  coJSONDetailedFormatNewLine = #13#10;
  coJSONDetailedFormatIndent = '  ';

  coJSONMaxLongWord: LongWord = LongWord(-1);

  coJSONMinInt64: Int64 = -9223372036854775808;
  coJSONMaxInt64: Int64 = 9223372036854775807;

type
  TJSONParser = class(TObject)
    public
      class function Unescape(const AJSONStringWithoutDelimiters: UnicodeString): UnicodeString;
      class function SeachEndQuotePos(const ACurrentChar: PWideChar; const ALastChar: PWideChar; out AEndQuoteChar: PWideChar): Boolean;
  end;

  TJSONMember = class(TObject)
    strict private
      fName: UnicodeString;

    public
      constructor Create(const AName: UnicodeString); virtual;

      procedure Reset(); virtual;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; reintroduce; virtual;
      function ToSimpleValue(): UnicodeString; virtual; abstract;

      property Name: UnicodeString read fName;
  end;

  TJSONMemberClass = class of TJSONMember;

  TJSONMemberListItem = class(TObject)
    strict private
      fMember: TJSONMember;  // member instance

    private
      // is member static?
      //
      // static means
      //  a) member not freed by list
      //  b) list item is only removed when Clear() is called with AFreeStaticItems = true
      fStatic: Boolean;

    public
      constructor Create(const AMember: TJSONMember; const AStatic: Boolean);
      destructor Destroy(); override;

      property Member: TJSONMember read fMember;
      property Static: Boolean read fStatic;
  end;

  TJSONMemberList = class(TObjectList)
    private
      function GetItem(const AIndex: Integer): TJSONMemberListItem;
      procedure SetItem(const AIndex: Integer; const AObject: TJSONMemberListItem);

    public
      function Retrieve(const AIndex: Integer; const AMemberClass: TJSONMemberClass = nil): TJSONMember;
      function Find(const AName: UnicodeString; const AMemberClass: TJSONMemberClass = nil): TJSONMember;
      function Add(const AMember: TJSONMember; const AStatic: Boolean = false): Integer; overload;
      function Add(const AMemberName: UnicodeString; const AMemberValue: Integer): Integer; overload;
      function Add(const AMemberName: UnicodeString; const AMemberValue: Boolean): Integer; overload;
      function Add(const AMemberName: UnicodeString; const AMemberValue: UnicodeString): Integer; overload;
      procedure Remove(const AMember: TJSONMember);
      procedure Clear(const AFreeStaticItems: Boolean); reintroduce;

      function Add(const AObject: TJSONMemberListItem): Integer; overload;
      function Extract(const AItem: TJSONMemberListItem): TJSONMemberListItem; overload;
      function Extract(const AMember: TJSONMember): TJSONMember; overload;
      procedure Insert(const AIndex: Integer; const AObject: TJSONMemberListItem);
      function First(): TJSONMemberListItem;
      Function Last(): TJSONMemberListItem;

      property Items[const AIndex: Integer]: TJSONMemberListItem read GetItem write SetItem; default;
  end;

  TJSONObject = class;

  TJSONObjectMember = class(TJSONMember)
    strict private

      function GetMembers(): TJSONMemberList;

    private
      fObject: TJSONObject;

    public
      constructor Create(const AName: UnicodeString); override;
      destructor Destroy(); override;

      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; override;
      function ToSimpleValue(): UnicodeString; override;
      function GetSimpleValue(const AMemberName: UnicodeString; out AValue: Integer): Boolean;

      property Members: TJSONMemberList read GetMembers;
  end;

  TJSONArray = class;
  TJSONValueList = class;

  TJSONArrayMember = class(TJSONMember)
    strict private
      function GetValues(): TJSONValueList;

    private
      fArray: TJSONArray;

    public
      constructor Create(const AName: UnicodeString); override;
      destructor Destroy(); override;

      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; override;
      function ToSimpleValue(): UnicodeString; override;

      property Values: TJSONValueList read GetValues;
  end;

  TJSONStringMember = class(TJSONMember)
    strict private
      fValue: UnicodeString;

    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; override;
      function ToSimpleValue(): UnicodeString; override;

      property Value: UnicodeString read fValue write fValue;
  end;

  TJSONFloatMember = class(TJSONMember)
    strict private
      fValue: Double;

    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; override;
      function ToSimpleValue(): UnicodeString; override;
      function ToLongWord(): LongWord;
      function ToInt64(): Int64;

      property Value: Double read fValue write fValue;
  end;

  TJSONBooleanMember = class(TJSONMember)
    strict private
      fValue: Boolean;

    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; override;
      function ToSimpleValue(): UnicodeString; override;

      property Value: Boolean read fValue write fValue;
  end;

  TJSONNullMember = class(TJSONMember)
    public
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0;
                        const AIncludeName: Boolean = true): UnicodeString; override;
      function ToSimpleValue(): UnicodeString; override;
  end;

  TJSONValue = class(TObject)
    protected
      function ParseValue(const ATrimmedValueStr: UnicodeString; out AErrorMessage: UnicodeString): TJSONValue;

    public
      procedure AfterConstruction(); override;

      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; reintroduce; virtual;

      procedure Reset(); virtual; abstract;
  end;

  TJSONValueList = class(TObjectList)
    private
      function GetItem(const AIndex: Integer): TJSONValue;
      procedure SetItem(const AIndex: Integer; const AObject: TJSONValue);

    public
      function Add(const AValue: UnicodeString): Integer; overload;
      procedure Add(const AValueList: TStrings); overload;

      function Add(const AObject: TJSONValue): Integer; overload;
      function Extract(const AItem: TJSONValue): TJSONValue;
      procedure Insert(const AIndex: Integer; const AObject: TJSONValue);
      function First(): TJSONValue;
      Function Last(): TJSONValue;

      property Items[const AIndex: Integer]: TJSONValue read GetItem write SetItem; default;
  end;

  TJSONString = class(TJSONValue)
    strict private
       fValue: UnicodeString;

    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; override;

      property Value: UnicodeString read fValue write fValue;
  end;

  TJSONBoolean = class(TJSONValue)
    strict private
      fValue: Boolean;

    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; override;

      property Value: Boolean read fValue write fValue;
  end;

  TJSONNumber = class(TJSONValue)
    strict private
      fValue: Double;

    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; override;

      property Value: Double read fValue write fValue;
  end;

  TJSONNull = class(TJSONValue)
    public
      procedure Reset(); override;
      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; override;
  end;

  TJSONObject = class(TJSONValue)
    strict private
      fMembers: TJSONMemberList;

    protected
      function AfterReadSucceeded(): Boolean; virtual;

      // method called in read method, override this method in derived classes
      // to provide e.g. an existing member instance instead of creating a new one
      function ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember; virtual;

      function ReadPtr(const ASerializedData: PWideChar;
                       out AErrorMessage: UnicodeString;
                       out ACharsRead: LongWord): Boolean;

    public
      constructor Create(); virtual;
      destructor Destroy(); override;

      procedure AfterConstruction(); override;

      procedure Reset(); override;

      function Read(const ASerializedData: UnicodeString;
                    out AErrorMessage: UnicodeString;
                    out ACharsRead: LongWord): Boolean;

      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; override;

      property Members: TJSONMemberList read fMembers;
  end;

  TJSONArray = class(TJSONValue)
    strict private
      fValues: TJSONValueList;

    protected
      function AfterReadSucceeded(): Boolean; virtual;

    public
      constructor Create();
      destructor Destroy(); override;

      procedure Reset(); override;

      function Read(const ASerializedData: PWideChar;
                    out AErrorMessage: UnicodeString;
                    out ACharsRead: LongWord): Boolean; overload;

      function Read(const ASerializedData: UnicodeString;
                    out AErrorMessage: UnicodeString): Boolean; overload;

      function ToString(const ACompactFormat: Boolean = true;
                        const ACompactFormatIndentSize: LongWord = 0): UnicodeString; override;

      property Values: TJSONValueList read fValues;
  end;

  TJSONValueKind = (
    vkInvalid,
    vkNull,
    vkBoolean,
    vkNumber,
    vkString,
    vkObject,
    vkArray
  );

  TJSONValueInfo = record
    Kind: TJSONValueKind;
    ValueAsNumber: Double;  // only valid when Kind = vkNumber
    ValueAsBoolean: Boolean;  // only valid when Kind = vkBoolean
    StringValue: UnicodeString; // only valid for Kind = vkString
  end;

  function GetValueInfo(const ATrimmedValue: UnicodeString): TJSONValueInfo;

implementation

uses
  SysUtils, // for Trim()
  StrUtils; // for DupeString()

var
  gFloatFormatSettings: TFormatSettings;

{$REGION ' Helpers '}

function GetValueInfo(const ATrimmedValue: UnicodeString): TJSONValueInfo;
var
  lFirstChar: WideChar;

begin
  result.Kind := vkInvalid;
  result.ValueAsNumber := 0;
  result.ValueAsBoolean := false;
  result.StringValue := '';

  if ATrimmedValue <> '' then begin

    if ATrimmedValue = coJSONReservedWordNull then
      result.Kind := vkNull
    else
    if (ATrimmedValue = coJSONReservedWordFalse) or (ATrimmedValue = coJSONReservedWordTrue) then begin
      result.Kind := vkBoolean;
      result.ValueAsBoolean := ATrimmedValue = coJSONReservedWordTrue;
    end else begin
      lFirstChar := ATrimmedValue[1];

      if lFirstChar = coJSONStringDelimiter then begin
        result.Kind := vkString;
        result.StringValue := Copy(ATrimmedValue, 2, Length(ATrimmedValue) - 2);

        result.StringValue := TJSONParser.Unescape(result.StringValue);
      end else
      if lFirstChar = coJSONBeginObjectCharacter then
        result.Kind := vkObject
      else
      if lFirstChar = coJSONBeginArrayCharacter then
        result.Kind := vkArray
      else
      if TryStrToFloat(ATrimmedValue, result.ValueAsNumber, gFloatFormatSettings) then
        result.Kind := vkNumber;
    end;
  end;
end;

{$ENDREGION}

{$REGION ' TJSONParser '}

class function TJSONParser.Unescape(const AJSONStringWithoutDelimiters: UnicodeString): UnicodeString;
var
  lCurrentChar: PWideChar;
  lProcessingEscapeSequence: Boolean;
  p, l: Integer;

begin
  result := AJSONStringWithoutDelimiters;

  if result <> '' then begin
    lCurrentChar := PWideChar(@result[1]);

    lProcessingEscapeSequence := false;
    l := Length(result);
    p := 0;
    while p < l do begin

      if lProcessingEscapeSequence then begin
        result := Copy(result, 1, p - 1) + Copy(result, p + 1, l);

        l := l - 1;
        lCurrentChar := PWideChar(@result[p]);
        p := p - 1;

        lProcessingEscapeSequence := false;
      end else
        lProcessingEscapeSequence := lCurrentChar^ = csJSONEscapeCharacter;

      Inc(lCurrentChar);
      p := p + 1;
    end;
  end;
end;

class function TJSONParser.SeachEndQuotePos(const ACurrentChar: PWideChar; const ALastChar: PWideChar; out AEndQuoteChar: PWideChar): Boolean;
var
  lCurrentChar: PWideChar;
  lProcessingEscapedChar: Boolean;

begin
  result := false;

  if ACurrentChar^= coJSONStringDelimiter then
  begin
    lCurrentChar := ACurrentChar + 1;

    lProcessingEscapedChar := false;

    // note: searching with simple escape support only

    while lCurrentChar <= ALastChar do begin

      if lCurrentChar^ = csJSONEscapeCharacter then begin
        if not lProcessingEscapedChar then
          lProcessingEscapedChar := true;
      end;

      if lCurrentChar^ = coJSONStringDelimiter then begin
        if not lProcessingEscapedChar then begin
          AEndQuoteChar := lCurrentChar;

          result := true;

          break;
        end;
      end;

      Inc(lCurrentChar);

      lProcessingEscapedChar := false;
    end;

  end;
end;

{$ENDREGION}

{$REGION ' TJSONMember '}

constructor TJSONMember.Create(const AName: UnicodeString);
begin
  inherited Create();

  fName := AName;
end;

procedure TJSONMember.Reset();
begin
  // override in derived classes to reset instance
end;

function TJSONMember.ToString(const ACompactFormat: Boolean = true;
                              const ACompactFormatIndentSize: LongWord = 0;
                              const AIncludeName: Boolean = true): UnicodeString;
begin
  if AIncludeName then begin
    if not ACompactFormat then
      result :=  DupeString(coJSONDetailedFormatIndent, ACompactFormatIndentSize)
    else
      result := '';

    result := result + coJSONStringDelimiter + fName+ coJSONStringDelimiter + coJSONMemberNameValueSeparator;

    if not ACompactFormat then
      result := result + ' ';
  end else
    result := '';
end;

{$ENDREGION}

{$REGION ' TJSONMemberListItem '}

constructor TJSONMemberListItem.Create(const AMember: TJSONMember; const AStatic: Boolean);
begin
  inherited Create();

  fMember := AMember;
  fStatic := AStatic;
end;

destructor TJSONMemberListItem.Destroy();
begin
  if not fStatic then
    fMember.Free;

  inherited;
end;

{$ENDREGION}

{$REGION ' TJSONMemberList '}

function TJSONMemberList.Find(const AName: UnicodeString; const AMemberClass: TJSONMemberClass = nil): TJSONMember;
var
  i, l: Integer;

begin
  result := nil;

  i := 0;
  l := Count;
  while i < l do begin
    result := GetItem(i).Member;

    if Assigned(result) and (result.Name = AName) then
      i := l
    else begin
      result := nil;

      i := i + 1;
    end;
  end;

  if Assigned(AMemberClass) and not (result is AMemberClass) then
    result := nil;
end;

function TJSONMemberList.Add(const AMember: TJSONMember; const AStatic: Boolean = false): Integer;
var
  lMemberListItem: TJSONMemberListItem;

begin
  lMemberListItem := TJSONMemberListItem.Create(AMember, AStatic);

  result := inherited Add(lMemberListItem);
end;

function TJSONMemberList.Add(const AMemberName: UnicodeString; const AMemberValue: Integer): Integer;
var
  lFloatMember: TJSONFloatMember;

begin
  lFloatMember := TJSONFloatMember.Create(AMemberName);
  lFloatMember.Value := AMemberValue;

  result := Add(lFloatMember, false);
end;

function TJSONMemberList.Add(const AMemberName: UnicodeString; const AMemberValue: Boolean): Integer;
var
  lBooleanMember: TJSONBooleanMember;

begin
  lBooleanMember := TJSONBooleanMember.Create(AMemberName);
  lBooleanMember.Value := AMemberValue;

  result := Add(lBooleanMember, false);
end;

function TJSONMemberList.Add(const AMemberName: UnicodeString; const AMemberValue: UnicodeString): Integer;
var
  lStringMember: TJSONStringMember;

begin
  lStringMember := TJSONStringMember.Create(AMemberName);
  lStringMember.Value := AMemberValue;

  result := Add(lStringMember, false);
end;

procedure TJSONMemberList.Clear(const AFreeStaticItems: Boolean);
var
  i: Integer;
  lItem: TJSONMemberListItem;

begin
  i := 0;
  while i < Count do begin
    lItem := GetItem(i);

    if (lItem.Static and AFreeStaticItems) or not lItem.Static then
      Delete(i)
    else
      i := i + 1;
  end;
end;

procedure TJSONMemberList.Remove(const AMember: TJSONMember);
var
  i: Integer;
  lItem: TJSONMemberListItem;

begin
  i := 0;
  while i < Count do begin
    lItem := GetItem(i);

    if lItem.Member = AMember then
      Delete(i)
    else
      i := i + 1;
  end;
end;

function TJSONMemberList.Retrieve(const AIndex: Integer; const AMemberClass: TJSONMemberClass = nil): TJSONMember;
var
  lItem: TJSONMemberListItem;

begin
  if (AIndex >= 0) and (AIndex < Count) then begin
    lItem := GetItem(AIndex);

    if Assigned(AMemberClass) then begin

      if lItem.Member is AMemberClass then
        result := lItem.Member
      else
        result := nil;

    end else
      result := lItem.Member;
  end else
    result := nil
end;

function TJSONMemberList.GetItem(const AIndex: Integer): TJSONMemberListItem;
begin
  result := TJSONMemberListItem(inherited GetItem(AIndex));
end;

procedure TJSONMemberList.SetItem(const AIndex: Integer; const AObject: TJSONMemberListItem);
begin
  inherited SetItem(AIndex, AObject);
end;

function TJSONMemberList.Add(const AObject: TJSONMemberListItem): Integer;
begin
  result := inherited Add(AObject);
end;

function TJSONMemberList.Extract(const AItem: TJSONMemberListItem): TJSONMemberListItem;
begin
  result := TJSONMemberListItem(inherited Extract(AItem));
end;

function TJSONMemberList.Extract(const AMember: TJSONMember): TJSONMember;
var
  i: Integer;
  lItem: TJSONMemberListItem;
  lMember: TJSONMember;

begin
  result := nil;

  i := 0;
  while i < Count do begin
    lItem := GetItem(i);

    lMember := lItem.Member;

    if lItem.Member = AMember then begin
      lItem := Extract(lItem);

      if Assigned(lItem) then begin
        lItem.fStatic := true; // instruct item to not freeing member on destruction

        result := lItem.Member;

        lItem.Free;
      end;

      break;
    end else
      i := i + 1;
  end;
end;

procedure TJSONMemberList.Insert(const AIndex: Integer; const AObject: TJSONMemberListItem);
begin
  inherited Insert(AIndex, AObject);
end;

function TJSONMemberList.First(): TJSONMemberListItem;
begin
  result := TJSONMemberListItem(inherited First());
end;

function TJSONMemberList.Last(): TJSONMemberListItem;
begin
  result := TJSONMemberListItem(inherited Last());
end;

{$ENDREGION}

{$REGION ' TJSONValue '}

procedure TJSONValue.AfterConstruction();
begin
  inherited;

  Reset();
end;

function TJSONValue.ToString(const ACompactFormat: Boolean = true;
                             const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
begin
  if not ACompactFormat then
    result := DupeString(coJSONDetailedFormatIndent, ACompactFormatIndentSize)
  else
    result := '';
end;

function TJSONValue.ParseValue(const ATrimmedValueStr: UnicodeString; out AErrorMessage: UnicodeString): TJSONValue;
var
  lValueInfo: TJSONValueInfo;
  lBooleanValue: TJSONBoolean;
  lNullValue: TJSONNull;
  lNumberValue: TJSONNumber;
  lObjectValue: TJSONObject;
  lArrayValue: TJSONArray;
  lStringValue: TJSONString;
  lCharsRead: LongWord;

begin
  result := nil;

  if ATrimmedValueStr <> '' then begin
    AErrorMessage := '';

    lValueInfo := GetValueInfo(ATrimmedValueStr);

    // object value
    if lValueInfo.Kind = vkObject then begin
      lObjectValue := TJSONObject.Create();

      if lObjectValue.Read(ATrimmedValueStr, AErrorMessage, lCharsRead) then
        result := lObjectValue
      else
        lObjectValue.Free;

    end else
    // array value
    if lValueInfo.Kind = vkArray then begin
      lArrayValue := TJSONArray.Create();

      if lArrayValue.Read(ATrimmedValueStr, AErrorMessage) then
        result := lArrayValue
      else
        lArrayValue.Free;

    end else
    // boolean value
    if lValueInfo.Kind = vkBoolean then begin
      lBooleanValue := TJSONBoolean.Create();
      lBooleanValue.Value := ATrimmedValueStr = coJSONReservedWordTrue;

      result := lBooleanValue;
    end else
    // null value
    if lValueInfo.Kind = vkNull then begin
      lNullValue := TJSONNull.Create();

      result := lNullValue;
    end else
    // number value
    if lValueInfo.Kind = vkNumber then begin
      lNumberValue := TJSONNumber.Create();
      lNumberValue.Value := lValueInfo.ValueAsNumber;

      result := lNumberValue;
    end else
    // string value
    if lValueInfo.Kind = vkString then begin
      lStringValue := TJSONString.Create();
      lStringValue.Value := lValueInfo.StringValue;

      result := lStringValue;
    end else
      AErrorMessage := 'Cannot parse value string to a known value kind';

  end else
    AErrorMessage := 'Empty value string not allowed';
end;

{$ENDREGION}

{$REGION ' TJSONValueList '}

function TJSONValueList.Add(const AValue: UnicodeString): Integer;
var
  lJSONString: TJSONString;

begin
  lJSONString := TJSONString.Create();
  lJSONString.Value := AValue;

  result := Add(lJSONString);
end;

procedure TJSONValueList.Add(const AValueList: TStrings);
var
  i: Integer;
  lJSONString: TJSONString;

begin
  i := 0;
  while i < AValueList.Count do begin
    lJSONString := TJSONString.Create();
    lJSONString.Value := AValueList[i];

    inherited Add(lJSONString);

    i := i + 1;
  end;
end;

function TJSONValueList.GetItem(const AIndex: Integer): TJSONValue;
begin
  result := TJSONValue(inherited GetItem(AIndex));
end;

procedure TJSONValueList.SetItem(const AIndex: Integer; const AObject: TJSONValue);
begin
  inherited SetItem(AIndex, AObject);
end;

function TJSONValueList.Add(const AObject: TJSONValue): Integer;
begin
  result := inherited Add(AObject);
end;

function TJSONValueList.Extract(const AItem: TJSONValue): TJSONValue;
begin
  result := TJSONValue(inherited Extract(AItem));
end;

procedure TJSONValueList.Insert(const AIndex: Integer; const AObject: TJSONValue);
begin
  inherited Insert(AIndex, AObject);
end;

function TJSONValueList.First(): TJSONValue;
begin
  result := TJSONValue(inherited First());
end;

function TJSONValueList.Last(): TJSONValue;
begin
  result := TJSONValue(inherited Last());
end;

{$ENDREGION}

{$REGION ' TJSONString '}

procedure TJSONString.Reset();
begin
  fValue := '';
end;

function TJSONString.ToString(const ACompactFormat: Boolean = true;
                              const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
begin
  result := inherited + coJSONStringDelimiter + fValue + coJSONStringDelimiter;
end;

{$ENDREGION}

{$REGION ' TJSONBoolean '}

procedure TJSONBoolean.Reset();
begin
  fValue := false;
end;

function TJSONBoolean.ToString(const ACompactFormat: Boolean = true;
                               const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
begin
  if fValue then
    result := inherited + coJSONReservedWordTrue
  else
    result := inherited + coJSONReservedWordFalse;
end;

{$ENDREGION}

{$REGION ' TJSONNumber '}

procedure TJSONNumber.Reset();
begin
  fValue := 0;
end;

function TJSONNumber.ToString(const ACompactFormat: Boolean = true;
                              const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
begin
  result := inherited + FloatToStr(fValue);
end;

{$ENDREGION}

{$REGION ' TJSONNull '}

procedure TJSONNull.Reset();
begin
  // nothing to reset, object represents "null" value
end;

function TJSONNull.ToString(const ACompactFormat: Boolean = true;
                            const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
begin
  result := inherited + coJSONReservedWordNull;
end;

{$ENDREGION}

{$REGION ' TJSONObject '}

constructor TJSONObject.Create();
begin
  inherited;

  fMembers := TJSONMemberList.Create(true);
end;

destructor TJSONObject.Destroy();
begin
  fMembers.Free;

  inherited;
end;

procedure TJSONObject.AfterConstruction();
begin
  inherited;

  Reset();
end;

procedure TJSONObject.Reset();
begin
  fMembers.Clear(false);
end;

function TJSONObject.Read(const ASerializedData: UnicodeString;
                          out AErrorMessage: UnicodeString;
                          out ACharsRead: LongWord): Boolean;
var
  lSerializedData: UnicodeString;
  lFirstChar: PWideChar;

begin
  result := false;

  Reset();

  lSerializedData := Trim(ASerializedData);

  if lSerializedData <> '' then begin
    lFirstChar := PWideChar(@lSerializedData[1]);

    if ReadPtr(lFirstChar, AErrorMessage, ACharsRead) then begin
      result := AfterReadSucceeded();
    end;
  end else
    AErrorMessage := 'Serialized data is empty';
end;

function TJSONObject.ReadPtr(const ASerializedData: PWideChar;
                             out AErrorMessage: UnicodeString;
                             out ACharsRead: LongWord): Boolean;
type
  TSyntaxElement = (
    seMemberValue,
    seMemberNameValueSeparator,
    seMemberSeparator,
    seClosingCurlyBracket
  );

var
  lCurrentChar, lLastChar, lMemberNameValueSeparatorChar, lEndQuoteChar: PWideChar;
  lMessageLength: Integer;
  lExpectedSyntaxElements: set of TSyntaxElement;
  lMemberName, lMemberValue, lStr, lErrorMessage: UnicodeString;
  lTempChar: WideChar;
  lExitProcessing: Boolean;
  lCharsRead: LongWord;
  lObjectMember: TJSONObjectMember;
  lBooleanMember: TJSONBooleanMember;
  lNullMember: TJSONNullMember;
  lFloatMember: TJSONFloatMember;
  lStringMember: TJSONStringMember;
  lArrayMember: TJSONArrayMember;
  lValueInfo: TJSONValueInfo;

  // this procedure works just like a macro, quick and dirty solution used to
  // use the same code in different branches below
  procedure AddMemberWithNonQuotedValue();
  begin
    if Assigned(lMemberNameValueSeparatorChar) then begin

      if lMemberNameValueSeparatorChar < lLastChar then begin

        lTempChar := lCurrentChar^;
        lCurrentChar^ := #0;

        lMemberValue := Trim(UnicodeString(lMemberNameValueSeparatorChar + 1));

        lCurrentChar^ := lTempChar;

        lValueInfo := GetValueInfo(lMemberValue);

        // boolean member
        if lValueInfo.Kind = vkBoolean then begin
          lBooleanMember := ProvideMember(lMemberName, TJSONBooleanMember) as TJSONBooleanMember;
          lBooleanMember.Value := lValueInfo.ValueAsBoolean;
        end else
        // null member
        if lValueInfo.Kind = vkNull then begin
          lNullMember := ProvideMember(lMemberName, TJSONNullMember) as TJSONNullMember;
        end else
        if lValueInfo.Kind = vkNumber then begin
          // floating point number member
          lFloatMember := ProvideMember(lMemberName, TJSONFloatMember) as TJSONFloatMember;
          lFloatMember.Value := lValueInfo.ValueAsNumber;
        end else begin
          AErrorMessage := 'Member value kind not allowed here';
          lExitProcessing := true;
        end;

        Exclude(lExpectedSyntaxElements, seMemberValue);
        lMemberNameValueSeparatorChar := nil;

       // not including member separator or closing curly bracket (depends on
       // where the function is called, see below) in expected syntax elements
       // because this branch is processing this separator

      end else begin
        AErrorMessage := 'Member name value separator as last character is not allowed';
        lExitProcessing := true;
      end;

    end else begin
      AErrorMessage := 'Member separator found but not expected at this point';
      lExitProcessing := true;
    end;
  end;

begin
  result := false;

  fMembers.Clear(false);

  lMessageLength := Length(ASerializedData);
  lCurrentChar := ASerializedData;

  if lMessageLength > 0 then begin
    lLastChar := lCurrentChar + lMessageLength - 1;

    lExpectedSyntaxElements := [];
    lExitProcessing := false;
    lMemberNameValueSeparatorChar := nil;
    lMemberName := '';

    lErrorMessage := 'Unknown error';

    while (lCurrentChar <= lLastChar) and not lExitProcessing do begin

      if lCurrentChar^ = coJSONBeginObjectCharacter then begin

        if seMemberValue in lExpectedSyntaxElements then begin
          // object member

          lObjectMember := ProvideMember(lMemberName, TJSONObjectMember) as TJSONObjectMember;

          if lObjectMember.fObject.ReadPtr(lCurrentChar, lErrorMessage, lCharsRead) then begin
            // do further processing on success
          end else
            lExitProcessing := true;

          // -1 due to incrementing of lCurrentChar on last step in loop
          lCurrentChar := lCurrentChar + lCharsRead - 1;

          Exclude(lExpectedSyntaxElements, seMemberValue);
          lMemberNameValueSeparatorChar := nil;

          Include(lExpectedSyntaxElements, seMemberSeparator);
        end else
          Include(lExpectedSyntaxElements, seClosingCurlyBracket);

      end else
      if lCurrentChar^ = coJSONEndObjectCharacter then begin

        if seClosingCurlyBracket in lExpectedSyntaxElements then begin

          Exclude(lExpectedSyntaxElements, seClosingCurlyBracket);

          if seMemberValue in lExpectedSyntaxElements then begin
            // so its a member value without quote chars, e.g. a number

            AddMemberWithNonQuotedValue();

            if not lExitProcessing then begin
              Include(lExpectedSyntaxElements, seMemberSeparator);

              result := true;
            end;
          end else
          if (seMemberSeparator in lExpectedSyntaxElements) or
             (lExpectedSyntaxElements = [])
          then begin
            // add additional checks

            result := true;
          end else
            lErrorMessage := coJSONEndObjectCharacter + ' found but not expected at this point';

        end else
          lErrorMessage := coJSONEndObjectCharacter + ' found but not expected at this point';

        // exit loop
        lExitProcessing := true;

      end else
      if lCurrentChar^ = coJSONBeginArrayCharacter then begin

        if seMemberValue in lExpectedSyntaxElements then begin
          // object member

          lArrayMember := ProvideMember(lMemberName, TJSONArrayMember) as TJSONArrayMember;

          if lArrayMember.fArray.Read(lCurrentChar, lErrorMessage, lCharsRead) then begin
            // do further processing on success
          end else
            lExitProcessing := true;

          // -1 due to incrementing of lCurrentChar on last step in loop
          lCurrentChar := lCurrentChar + lCharsRead - 1;

          Exclude(lExpectedSyntaxElements, seMemberValue);
          lMemberNameValueSeparatorChar := nil;

          Include(lExpectedSyntaxElements, seMemberSeparator);
        end else begin
          lErrorMessage := coJSONBeginArrayCharacter + ' found but not expected at this point';
          lExitProcessing := true;
        end;

      end else
      if lCurrentChar^ = coJSONStringDelimiter then begin

        if TJSONParser.SeachEndQuotePos(lCurrentChar, lLastChar, lEndQuoteChar) then begin
          Inc(lCurrentChar);

          lTempChar := lEndQuoteChar^;
          lEndQuoteChar^ := #0;

          if seMemberValue in lExpectedSyntaxElements then begin
            lMemberValue := lCurrentChar;

            // string member
            lStringMember := ProvideMember(lMemberName, TJSONStringMember) as TJSONStringMember;

            lStringMember.Value := TJSONParser.Unescape(lMemberValue);

            Exclude(lExpectedSyntaxElements, seMemberValue);
            lMemberNameValueSeparatorChar := nil;

            Include(lExpectedSyntaxElements, seMemberSeparator);
          end else begin
            lMemberName := lCurrentChar;

            Include(lExpectedSyntaxElements, seMemberNameValueSeparator);
          end;

           lEndQuoteChar^ := lTempChar;

           lCurrentChar := lEndQuoteChar;

        end else begin
          lErrorMessage := 'Could not find closing quote char';
          lExitProcessing := true;
        end;

      end else
      if lCurrentChar^ = coJSONMemberNameValueSeparator then begin

        if seMemberNameValueSeparator in lExpectedSyntaxElements then begin
          lMemberNameValueSeparatorChar := lCurrentChar;

          Exclude(lExpectedSyntaxElements, seMemberNameValueSeparator);
          Include(lExpectedSyntaxElements, seMemberValue);
        end else begin
          lErrorMessage := 'Member name value separator found but not expected at this point';
          lExitProcessing := true;
        end;

      end else
      if lCurrentChar^ = coJSONMemberSeparator then begin

        if seMemberSeparator in lExpectedSyntaxElements then begin
          Exclude(lExpectedSyntaxElements, seMemberSeparator);
        end else
        if seMemberValue in lExpectedSyntaxElements then begin
          // so its a member value without quote chars, e.g. a number

          AddMemberWithNonQuotedValue();

        end else begin
          lErrorMessage := 'Member separator found but not expected at this point';
          lExitProcessing := true;
        end;

      end;

      Inc(lCurrentChar);
    end;

  end else
    lErrorMessage := 'Serialized data is empty';

  if not result then begin
    // append serialized data to error message for better debugging
    lStr := Trim(UnicodeString(lCurrentChar));

    if lStr <> '' then
      lErrorMessage := lErrorMessage + #13#10 +
                       '...' + Trim(UnicodeString(lCurrentChar));

    AErrorMessage := lErrorMessage;
  end;

  ACharsRead := lCurrentChar - ASerializedData;
end;

function TJSONObject.ToString(const ACompactFormat: Boolean = true;
                              const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
var
  i, l: Integer;
  lMemberListItem: TJSONMemberListItem;
  lMember: TJSONMember;

begin
  result := inherited + coJSONBeginObjectCharacter;

  if not ACompactFormat then
    result := result + coJSONDetailedFormatNewLine;

  l := Members.Count;
  i := 0;
  while i < l do begin
    lMemberListItem := fMembers[i];

    if Assigned(lMemberListItem.Member) then begin
      lMember := lMemberListItem.Member;

      result := result + lMember.ToString(ACompactFormat, ACompactFormatIndentSize + 1);

      if (l > 1) and (i < l - 1) then
        result := result + coJSONMemberSeparator;

      if not ACompactFormat then
        result := result + coJSONDetailedFormatNewLine;
    end;

    i := i + 1;
  end;

  if not ACompactFormat then
    result := result + DupeString(coJSONDetailedFormatIndent, ACompactFormatIndentSize);

  result := result + coJSONEndObjectCharacter;
end;

function TJSONObject.AfterReadSucceeded(): Boolean;
begin
  // override in derived classes to some post processing after the object has
  // been read from serialized JSON data

  result := true;
end;

function TJSONObject.ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember;
begin
  result := AMemberClass.Create(AName);

  fMembers.Add(result, false);
end;

{$ENDREGION}

{$REGION ' TJSONArray '}

constructor TJSONArray.Create();
begin
  inherited;

  fValues := TJSONValueList.Create(true);
end;

destructor TJSONArray.Destroy();
begin
  fValues.Free;

  inherited;
end;

procedure TJSONArray.Reset();
begin
  fValues.Clear();
end;

function TJSONArray.AfterReadSucceeded(): Boolean;
begin
  // override in derived classes to some post processing after the object has
  // been read from serialized JSON data

  result := true;
end;

function TJSONArray.Read(const ASerializedData: PWideChar;
                         out AErrorMessage: UnicodeString;
                         out ACharsRead: LongWord): Boolean;
type
  TSyntaxElement = (
    seStringDelimiter,
    seValue,
    seValueSeparator,
    seOpeningSquareBracket,
    seClosingSquareBracket
  );

var
  lCurrentChar, lLastChar, lValueFirstChar, lEndQuoteChar: PWideChar;
  lMessageLength: Integer;
  lExpectedSyntaxElements: set of TSyntaxElement;
  lValue, lStr: UnicodeString;
  lTempChar: WideChar;
  lExitProcessing: Boolean;
  lCharsRead: LongWord;
  lStringValue: TJSONString;
  lJSONValue: TJSONValue;
  lArrayValue: TJSONArray;
  lObjectValue: TJSONObject;

begin
  result := false;

  fValues.Clear();

  lMessageLength := Length(ASerializedData);
  lCurrentChar := ASerializedData;

  if lMessageLength > 0 then begin
    lLastChar := lCurrentChar + lMessageLength - 1;

    lExpectedSyntaxElements := [seOpeningSquareBracket];
    lExitProcessing := false;
    lValueFirstChar := nil;

    AErrorMessage := 'Unknown error';

    while (lCurrentChar <= lLastChar) and not lExitProcessing do begin

      if lCurrentChar^ = coJSONBeginArrayCharacter then begin

        if seValue in lExpectedSyntaxElements then begin
          lArrayValue := TJSONArray.Create();

          if lArrayValue.Read(lCurrentChar, AErrorMessage, lCharsRead) then begin
            fValues.Add(lArrayValue)
          end else begin
            lArrayValue.Free;

            lExitProcessing := true;
          end;

          // -1 due to incrementing of lCurrentChar on last step in loop
          lCurrentChar := lCurrentChar + lCharsRead - 1;

          Exclude(lExpectedSyntaxElements, seValue);
          lValueFirstChar := nil;

          Include(lExpectedSyntaxElements, seValueSeparator);

        end else
        if seOpeningSquareBracket in lExpectedSyntaxElements then begin
          Exclude(lExpectedSyntaxElements, seOpeningSquareBracket);
          Include(lExpectedSyntaxElements, seClosingSquareBracket);
          Include(lExpectedSyntaxElements, seStringDelimiter);
          Include(lExpectedSyntaxElements, seValue);

        end else begin
          AErrorMessage := 'Opening square bracket found but not expected at this point';
          lExitProcessing := true;
        end;

      end else
      if lCurrentChar^ = coJSONEndArrayCharacter then begin

        if seValue in lExpectedSyntaxElements then begin

          if Assigned(lValueFirstChar) then begin
            // so its a member value without quote chars, e.g. a number

            lTempChar := lCurrentChar^;
            lCurrentChar^ := #0;

            lValue := Trim(UnicodeString(lValueFirstChar));

            lCurrentChar^ := lTempChar;

            if lValue <> '' then begin

              lJSONValue := ParseValue(lValue, AErrorMessage);
              if Assigned(lJSONValue) then
                fValues.Add(lJSONValue)
              else begin
                lJSONValue.Free;

                lCurrentChar := lValueFirstChar;
                lExitProcessing := true;
              end;

            end else
            if fValues.Count > 0 then begin
              // only return error when more than one value is present to ensure
              // empty array with e.g. spaces is not recognirzed as empty value
              AErrorMessage := 'Empty value not allowed';
              lExitProcessing := true;
            end;

          end;

          Exclude(lExpectedSyntaxElements, seValue);
        end;

        if not lExitProcessing then begin

          if seClosingSquareBracket in lExpectedSyntaxElements then begin
            // add additional checks

            result := true;
          end else
            AErrorMessage := coJSONEndArrayCharacter + ' found but not expected at this point';

        end;

        // exit loop
        lExitProcessing := true;

      end else
      if lCurrentChar^ = coJSONBeginObjectCharacter then begin

        if seValue in lExpectedSyntaxElements then begin
          lObjectValue := TJSONObject.Create();

          if lObjectValue.ReadPtr(lCurrentChar, AErrorMessage, lCharsRead) then begin
            fValues.Add(lObjectValue)
          end else begin
            lObjectValue.Free;

            lExitProcessing := true;
          end;

          // -1 due to incrementing of lCurrentChar on last step in loop
          lCurrentChar := lCurrentChar + lCharsRead - 1;

          Exclude(lExpectedSyntaxElements, seValue);
          lValueFirstChar := nil;

          Include(lExpectedSyntaxElements, seValueSeparator);
          Include(lExpectedSyntaxElements, seClosingSquareBracket);

        end else begin
          AErrorMessage := 'Opening curly bracket found but not expected at this point';
          lExitProcessing := true;
        end;

      end else
      if lCurrentChar^ = coJSONStringDelimiter then begin

        if seStringDelimiter in lExpectedSyntaxElements then begin
          Exclude(lExpectedSyntaxElements, seStringDelimiter);

          if seValue in lExpectedSyntaxElements then begin
            Exclude(lExpectedSyntaxElements, seValue);

            if TJSONParser.SeachEndQuotePos(lCurrentChar, lLastChar, lEndQuoteChar) then begin
              Inc(lCurrentChar);

              lTempChar := lEndQuoteChar^;
              lEndQuoteChar^ := #0;

              lStringValue := TJSONString.Create();
              lStringValue.Value := TJSONParser.Unescape(lCurrentChar);

              fValues.Add(lStringValue);

              lEndQuoteChar^ := lTempChar;

              lCurrentChar := lEndQuoteChar;

              Include(lExpectedSyntaxElements, seValueSeparator);
              Include(lExpectedSyntaxElements, seClosingSquareBracket);

              lValueFirstChar := nil;

            end else begin
              AErrorMessage := 'Could not find closing quote char';
              lExitProcessing := true;
            end;

          end else begin
            AErrorMessage := 'No value expected at this point';
            lExitProcessing := true;
          end;

        end else begin
          AErrorMessage := 'String delimiter found but not expected at this point';
          lExitProcessing := true;
        end;

      end else
      if lCurrentChar^ = coJSONMemberSeparator then begin

        if seValueSeparator in lExpectedSyntaxElements then begin
          Exclude(lExpectedSyntaxElements, seClosingSquareBracket);

          if seValue in lExpectedSyntaxElements then begin
            // so its a member value without quote chars, e.g. a number

            if Assigned(lValueFirstChar) then begin

              lTempChar := lCurrentChar^;
              lCurrentChar^ := #0;

              lValue := Trim(UnicodeString(lValueFirstChar));

              lCurrentChar^ := lTempChar;

              lJSONValue := ParseValue(lValue, AErrorMessage);
              if Assigned(lJSONValue) then
                fValues.Add(lJSONValue)
              else begin
                lJSONValue.Free;

                lCurrentChar := lValueFirstChar;
                lExitProcessing := true;
              end;

            end else begin
              AErrorMessage := 'Unknown expected error (code 1)';
              lExitProcessing := true;
            end;

          end else begin
            // ... value must have been parsed in string quote case before

            Include(lExpectedSyntaxElements, seValue);
          end;

          Include(lExpectedSyntaxElements, seStringDelimiter);

          lValueFirstChar := nil;

        end else begin
          AErrorMessage := 'Member separator found but not expected at this point';
          lExitProcessing := true;
        end;

      end else begin

        if seValue in lExpectedSyntaxElements then begin

          if not Assigned(lValueFirstChar) then begin
            lValueFirstChar := lCurrentChar;

            // following call needed when e.g. when first value is unquoted
            Include(lExpectedSyntaxElements, seValueSeparator);

            // following call neede when e.g. array with only unquoted values
            Include(lExpectedSyntaxElements, seClosingSquareBracket);
          end;

        end;

      end;

      Inc(lCurrentChar);
    end;

  end else
    AErrorMessage := 'Serialized data is empty';

  if not result then begin
    // append serialized data to error message for better debugging
    lStr := Trim(UnicodeString(lCurrentChar));

    if lStr <> '' then
      AErrorMessage := AErrorMessage + #13#10 +
                       '...' + Trim(UnicodeString(lCurrentChar));
  end;

  ACharsRead := lCurrentChar - ASerializedData;
end;

function TJSONArray.Read(const ASerializedData: UnicodeString;
                         out AErrorMessage: UnicodeString): Boolean;
var
  lSerializedData: UnicodeString;
  lFirstChar: PWideChar;
  lCharsRead: LongWord;

begin
  result := false;

  Reset();

  lSerializedData := Trim(ASerializedData);

  if lSerializedData <> '' then begin
    lFirstChar := PWideChar(@lSerializedData[1]);

    if Read(lFirstChar, AErrorMessage, lCharsRead) then begin
      result := AfterReadSucceeded();
    end;
  end else
    AErrorMessage := 'Serialized data is empty';
end;

function TJSONArray.ToString(const ACompactFormat: Boolean = true;
                             const ACompactFormatIndentSize: LongWord = 0): UnicodeString;
var
  i, l: Integer;
  lValue: TJSONValue;

begin
  result := inherited + coJSONBeginArrayCharacter;

  if not ACompactFormat then
    result := result + coJSONDetailedFormatNewLine;

  l := fValues.Count;
  i := 0;
  while i < l do begin
    lValue := fValues[i];

    result := result + lValue.ToString(ACompactFormat, ACompactFormatIndentSize + 1);

    if (l > 1) and (i < l - 1) then
      result := result + coJSONMemberSeparator;

    if not ACompactFormat then
      result := result + coJSONDetailedFormatNewLine;

    i := i + 1;
  end;

  if not ACompactFormat then
    result := result + DupeString(coJSONDetailedFormatIndent, ACompactFormatIndentSize);

  result := result + coJSONEndArrayCharacter;
end;

{$ENDREGION}

{$REGION ' TJSONObjectMember '}

constructor TJSONObjectMember.Create(const AName: UnicodeString);
begin
  inherited;

  fObject := TJSONObject.Create();
end;

destructor TJSONObjectMember.Destroy();
begin
  fObject.Free;

  inherited;
end;

procedure TJSONObjectMember.Reset();
begin
  inherited;

  fObject.Reset();
end;

function TJSONObjectMember.ToString(const ACompactFormat: Boolean = true;
                                    const ACompactFormatIndentSize: LongWord = 0;
                                    const AIncludeName: Boolean = true): UnicodeString;
begin
  result := inherited ToString(ACompactFormat, ACompactFormatIndentSize, AIncludeName);

  if not ACompactFormat then
    result := result + coJSONDetailedFormatNewLine;

  result := result + fObject.ToString(ACompactFormat, ACompactFormatIndentSize);
end;

function TJSONObjectMember.ToSimpleValue(): UnicodeString;
begin
  result := 'object';
end;

function TJSONObjectMember.GetMembers(): TJSONMemberList;
begin
  result := fObject.Members;
end;

function TJSONObjectMember.GetSimpleValue(const AMemberName: UnicodeString; out AValue: Integer): Boolean;
var
  lMember: TJSONMember;

begin
  lMember := fObject.Members.Find(AMemberName, TJSONMember);
  if lMember is TJSONFloatMember then begin
    AValue := Round(TJSONFloatMember(lMember).Value);

    result := true;
  end
  else
    result := false;
end;

{$ENDREGION}

{$REGION ' TJSONArrayMember '}

constructor TJSONArrayMember.Create(const AName: UnicodeString);
begin
  inherited;

  fArray := TJSONArray.Create();
end;

destructor TJSONArrayMember.Destroy();
begin
  fArray.Free;

  inherited;
end;

procedure TJSONArrayMember.Reset();
begin
  inherited;

  fArray.Reset();
end;

function TJSONArrayMember.ToString(const ACompactFormat: Boolean = true;
                                   const ACompactFormatIndentSize: LongWord = 0;
                                   const AIncludeName: Boolean = true): UnicodeString;
begin
  result := inherited ToString(ACompactFormat, ACompactFormatIndentSize, AIncludeName);

  if not ACompactFormat then
    result := result + coJSONDetailedFormatNewLine;

  result := result + fArray.ToString(ACompactFormat, ACompactFormatIndentSize);
end;

function TJSONArrayMember.ToSimpleValue(): UnicodeString;
var
  i, l: Integer;
  v: TJSONValue;

begin
  result := '';

  l := fArray.Values.Count - 1;
  for i := 0 to l do begin
    v := fArray.Values[i];

    if i > 0 then
      result := result + ', ';

    if v is TJSONString then
      result := result + TJSONString(v).Value
    else
      result := result + '?';
  end;
end;

function TJSONArrayMember.GetValues(): TJSONValueList;
begin
  result := fArray.Values;
end;

{$ENDREGION}

{$REGION ' TJSONStringMember '}

procedure TJSONStringMember.Reset();
begin
  inherited;

  fValue := '';
end;

function TJSONStringMember.ToString(const ACompactFormat: Boolean = true;
                                    const ACompactFormatIndentSize: LongWord = 0;
                                    const AIncludeName: Boolean = true): UnicodeString;
begin
  result := inherited ToString(ACompactFormat, ACompactFormatIndentSize, AIncludeName) +
            coJSONStringDelimiter +
            fValue +
            coJSONStringDelimiter;
end;

function TJSONStringMember.ToSimpleValue(): UnicodeString;
begin
  result := fValue;
end;

{$ENDREGION}

{$REGION ' TJSONFloatMember '}

procedure TJSONFloatMember.Reset();
begin
  inherited;

  fValue := 0;
end;

function TJSONFloatMember.ToString(const ACompactFormat: Boolean = true;
                                   const ACompactFormatIndentSize: LongWord = 0;
                                   const AIncludeName: Boolean = true): UnicodeString;
begin
  result := inherited ToString(ACompactFormat, ACompactFormatIndentSize, AIncludeName) + FloatToStr(fValue);
end;

function TJSONFloatMember.ToSimpleValue(): UnicodeString;
begin
  result := FloatToStr(fValue);
end;

function TJSONFloatMember.ToLongWord(): LongWord;
var
  lResult: Int64;

begin
  lResult := Round(fValue); // use of Round() as it is faster than Trunc()

  // range checks to avoid ERangeError
  if lResult < 0 then
    lResult := 0;

  if lResult > coJSONMaxLongWord then
   lResult := coJSONMaxLongWord;

  result := lResult;
end;

function TJSONFloatMember.ToInt64(): Int64;
var
  lResult: Int64;

begin
  lResult := Round(fValue); // use of Round() as it is faster than Trunc()

  // range checks to avoid ERangeError
  if lResult < coJSONMinInt64 then
    lResult := coJSONMinInt64;

  if lResult > coJSONMaxInt64 then
   lResult := coJSONMaxInt64;

  result := lResult;
end;

{$ENDREGION}

{$REGION ' TJSONBooleanMember '}

procedure TJSONBooleanMember.Reset();
begin
  inherited;

  fValue := false;
end;

function TJSONBooleanMember.ToString(const ACompactFormat: Boolean = true;
                                     const ACompactFormatIndentSize: LongWord = 0;
                                     const AIncludeName: Boolean = true): UnicodeString;
begin
  result := inherited ToString(ACompactFormat, ACompactFormatIndentSize, AIncludeName);

  if fValue then
    result := result + coJSONReservedWordTrue
  else
    result := result + coJSONReservedWordFalse;
end;

function TJSONBooleanMember.ToSimpleValue(): UnicodeString;
begin
  if fValue then
    result := coJSONReservedWordTrue
  else
    result := coJSONReservedWordFalse;
end;

{$ENDREGION}

{$REGION ' TJSONNullMember '}

function TJSONNullMember.ToString(const ACompactFormat: Boolean = true;
                                  const ACompactFormatIndentSize: LongWord = 0;
                                  const AIncludeName: Boolean = true): UnicodeString;
begin
  result := inherited ToString(ACompactFormat, ACompactFormatIndentSize, AIncludeName) + coJSONReservedWordNull;
end;

function TJSONNullMember.ToSimpleValue(): UnicodeString;
begin
  result := coJSONReservedWordNull;
end;

{$ENDREGION}

begin
  gFloatFormatSettings := DefaultFormatSettings;
  gFloatFormatSettings.DecimalSeparator := '.';
end.
