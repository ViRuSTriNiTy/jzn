//
// JSON RPC classes
//
// $Id$
//

unit JSONRPC;

interface

uses
  Classes, // for TStringStream
  SyncObjs, // for TEvent
  Contnrs, // for TObjectList
  JSON, // for TJSON classes
  LNET, // for TLTCP
  LEvents;  // for TLSelectEventer

type
  TJSONRPCObject = class(TJSONObject)
    strict private
      fVersion: TJSONStringMember;

      function GetVersion(): UnicodeString;
      procedure SetVersion(const AValue: UnicodeString);

    protected
      function ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember; override;

    public
      constructor Create(); override;
      destructor Destroy(); override;

      procedure Reset(); override;

      property Version: UnicodeString read GetVersion write SetVersion;
  end;

  TJSONRPCRequest = class(TJSONRPCObject)
    strict private
      fID: TJSONStringMember;
      fMethod: TJSONStringMember;
      fParams: TJSONObjectMember;

      function GetID(): UnicodeString;
      function GetMethod(): UnicodeString;
      procedure SetID(const AValue: UnicodeString);
      procedure SetMethod(const AValue: UnicodeString);

    protected
      function ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember; override;

    public
      constructor Create(); override;
      destructor Destroy(); override;

      procedure Reset(); override;

      property ID: UnicodeString read GetID write SetID;
      property Method: UnicodeString read GetMethod write SetMethod;
      property Params: TJSONObjectMember read fParams;
  end;

  TJSONRPCResponse = class(TJSONRPCObject)
    strict private
      fID: TJSONStringMember;
      fResult: TJSONMember;

      function GetID(): UnicodeString;
      procedure SetID(const AValue: UnicodeString);

    protected
      function ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember; override;

    public
      constructor Create(); override;
      destructor Destroy(); override;
      procedure Reset(); override;

      property ID: UnicodeString read GetID write SetID;
      property Result: TJSONMember read fResult;
  end;

  TJSONRPCResponseList = class(TObjectList)
    strict private
      fMaxItemCount: Integer;

      function GetItem(const AIndex: Integer): TJSONRPCResponse;
      procedure SetItem(const AIndex: Integer; const AObject: TJSONRPCResponse);

    public
      constructor Create(const AMaxItemCount: Integer);

      function Add(const AObject: TJSONRPCResponse): Integer;
      function Extract(const AItem: TJSONRPCResponse): TJSONRPCResponse;
      procedure Insert(const AIndex: Integer; const AObject: TJSONRPCResponse);
      function First(): TJSONRPCResponse;
      function Last(): TJSONRPCResponse;

      function Find(const AResponseID: UnicodeString; out AItem: TJSONRPCResponse): Boolean;

      property Items[const AIndex: Integer]: TJSONRPCResponse read GetItem write SetItem; default;
  end;

  TJSONRPCClient = class(TObject)
    strict private
      fCancelSendRequestEvent: TEvent;
      fTCPClient: TLTCP;
      fTCPEventer: TLSelectEventer;
      fResponseStream: TStringStream;
      fResponseObjectBuffer: TJSONRPCResponseList;

      function GetConnected(): Boolean;
      function GetHost(): UnicodeString;
      function GetPort(): LongWord;
      function GetResponseObjectBufferSize(): Integer;

      procedure ConnectionEstablished(ASocket: TLSocket);
      procedure ConnectionError(const AMessage: String; ASocket: TLSocket);
      procedure ConnectionDataReadyToReceive(ASocket: TLSocket);

    public
      constructor Create(const ACancelSendRequestEvent: TEvent);
      destructor Destroy(); override;

      procedure ProcessEvents();

      function Connect(const AHost: UnicodeString; const APort: LongWord): Boolean;
      procedure Disconnect();

      function SendRequest(const ARequest: TJSONRPCRequest;
                           const AResponseTimeout: Integer;
                           out AResponse: TJSONRPCResponse;
                           var AErrorMessage: UnicodeString): Boolean;

      property Connected: Boolean read GetConnected;
      property Host: UnicodeString read GetHost;
      property Port: LongWord read GetPort;
      property ResponseObjectBufferSize: Integer read GetResponseObjectBufferSize;
  end;

implementation

uses
  Windows, // for CopyMemory()
  LazUTF8, // for UTF8...() functions
  SySUtils; // for IntToStr()

{$REGION ' TJSONRPCObject '}

constructor TJSONRPCObject.Create();
begin
  inherited;

  fVersion := TJSONStringMember.Create('jsonrpc');
  Members.Add(fVersion, true);
end;

destructor TJSONRPCObject.Destroy();
begin
  Members.Remove(fVersion);
  fVersion.Free;

  inherited;
end;

function TJSONRPCObject.GetVersion(): UnicodeString;
begin
  result := fVersion.Value;
end;

procedure TJSONRPCObject.SetVersion(const AValue: UnicodeString);
begin
  fVersion.Value := AValue;
end;

procedure TJSONRPCObject.Reset();
begin
  inherited;

  fVersion.Reset();
end;

function TJSONRPCObject.ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember;
begin
  if (AName = fVersion.Name) and (fVersion is AMemberClass) then begin
    result := fVersion
  end else
    result := inherited;
end;

{$ENDREGION}

{$REGION ' TJSONRPCRequest '}

constructor TJSONRPCRequest.Create();
begin
  inherited;

  fID := TJSONStringMember.Create('id');
  Members.Add(fID, true);

  fMethod := TJSONStringMember.Create('method');
  Members.Add(fMethod, true);

  fParams := TJSONObjectMember.Create('params');
  Members.Add(fParams, true);
end;

destructor TJSONRPCRequest.Destroy();
begin
  Members.Remove(fParams);
  fParams.Free;

  Members.Remove(fMethod);
  fMethod.Free;

  Members.Remove(fID);
  fID.Free;

  inherited;
end;

procedure TJSONRPCRequest.Reset();
begin
  inherited;

  fID.Reset();
  fMethod.Reset();
  fParams.Reset();
end;

function TJSONRPCRequest.ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember;
begin
  if (AName = fParams.Name) and (fParams is AMemberClass) then begin
    result := fParams
  end else
  if (AName = fMethod.Name) and (fMethod is AMemberClass) then begin
    result := fMethod
  end else
  if (AName = fID.Name) and (fID is AMemberClass) then begin
    result := fID
  end else
    result := inherited;
end;

function TJSONRPCRequest.GetID(): UnicodeString;
begin
  result := fID.Value;
end;

procedure TJSONRPCRequest.SetID(const AValue: UnicodeString);
begin
  fID.Value := AValue;
end;

function TJSONRPCRequest.GetMethod(): UnicodeString;
begin
  result := fMethod.Value;
end;

procedure TJSONRPCRequest.SetMethod(const AValue: UnicodeString);
begin
  fMethod.Value := AValue;
end;

{$ENDREGION}

{$REGION ' TJSONRPCResponse '}

constructor TJSONRPCResponse.Create();
begin
  inherited;

  fID := TJSONStringMember.Create('id');
  Members.Add(fID, true);

  fResult := TJSONObjectMember.Create('result');
  Members.Add(fResult, true);
end;

destructor TJSONRPCResponse.Destroy();
begin
  Members.Remove(fResult);
  fResult.Free;

  Members.Remove(fID);
  fID.Free;

  inherited;
end;

procedure TJSONRPCResponse.Reset();
begin
  inherited;

  fID.Reset();
  fResult.Reset();
end;

function TJSONRPCResponse.ProvideMember(const AName: UnicodeString; const AMemberClass: TJSONMemberClass): TJSONMember;
begin
  if AName = 'result' then begin
    if not (fResult is AMemberClass) then begin
      // replace result object when needed because result can be of any type
      Members.Remove(fResult);
      fResult.Free;

      fResult := AMemberClass.Create('result');
      Members.Add(fResult, true);
    end;

    result := fResult
  end else
  if (AName = fID.Name) and (fID is AMemberClass) then begin
    result := fID
  end else
    result := inherited;
end;

function TJSONRPCResponse.GetID(): UnicodeString;
begin
  result := fID.Value;
end;

procedure TJSONRPCResponse.SetID(const AValue: UnicodeString);
begin
  fID.Value := AValue;
end;

{$ENDREGION}

{$REGION ' TJSONRPCResponseList '}

constructor TJSONRPCResponseList.Create(const AMaxItemCount: Integer);
begin
  inherited Create(true);

  fMaxItemCount := AMaxItemCount;
end;

function TJSONRPCResponseList.GetItem(const AIndex: Integer): TJSONRPCResponse;
begin
  result := TJSONRPCResponse(inherited GetItem(AIndex));
end;

procedure TJSONRPCResponseList.SetItem(const AIndex: Integer; const AObject: TJSONRPCResponse);
begin
  inherited SetItem(AIndex, AObject);
end;

function TJSONRPCResponseList.Add(const AObject: TJSONRPCResponse): Integer;
begin
  result := inherited Add(AObject);

  while Count > fMaxItemCount do
    Delete(0);
end;

function TJSONRPCResponseList.Extract(const AItem: TJSONRPCResponse): TJSONRPCResponse;
begin
  result := TJSONRPCResponse(inherited Extract(AItem));
end;

procedure TJSONRPCResponseList.Insert(const AIndex: Integer; const AObject: TJSONRPCResponse);
begin
  inherited Insert(AIndex, AObject);

  while Count > fMaxItemCount do
    Delete(0);
end;

function TJSONRPCResponseList.First(): TJSONRPCResponse;
begin
  result := TJSONRPCResponse(inherited First());
end;

function TJSONRPCResponseList.Last(): TJSONRPCResponse;
begin
  result := TJSONRPCResponse(inherited Last());
end;

function TJSONRPCResponseList.Find(const AResponseID: UnicodeString; out AItem: TJSONRPCResponse): Boolean;
var
  i, l: Integer;

begin
  AItem := nil;

  l := Count - 1;
  for i := 0 to l do begin
    AItem := GetItem(i);

    if AItem.ID = AResponseID then
      exit(true);
  end;

  result := false;
end;

{$ENDREGION}

{$REGION ' TJSONRPCClient '}

constructor TJSONRPCClient.Create(const ACancelSendRequestEvent: TEvent);
begin
  inherited Create();

  fCancelSendRequestEvent := ACancelSendRequestEvent;

  fTCPEventer := TLSelectEventer.Create();
  fTCPEventer.Timeout := 0; // no timeout to avoid blocking when CallAction() is called

  fTCPClient := TLTCP.Create(nil);
  fTCPClient.Eventer := fTCPEventer;
  fTCPClient.OnError := @ConnectionError;
  fTCPClient.OnConnect := @ConnectionEstablished;
  fTCPClient.OnReceive := @ConnectionDataReadyToReceive;

  fResponseStream := TStringStream.Create('');

  // buffer a maximum of 10 object then begin discarding old object,
  // this particular important as Kodi sends notifications (e.g. OnPlay) which
  // in return would bloat the buffer when no limit is set
  fResponseObjectBuffer := TJSONRPCResponseList.Create(10);
end;

destructor TJSONRPCClient.Destroy();
begin
  fResponseObjectBuffer.Free;
  fResponseStream.Free;

  fTCPClient.Free;
  fTCPEventer.Free;

  inherited;
end;

procedure TJSONRPCClient.ProcessEvents();
begin
  fTCPEventer.CallAction();
end;

procedure TJSONRPCClient.ConnectionEstablished(ASocket: TLSocket);
begin
  fResponseStream.Size := 0;
  fResponseObjectBuffer.Clear();
end;

procedure TJSONRPCClient.ConnectionError(const AMessage: String; ASocket: TLSocket);
begin
  fResponseStream.Size := 0;
  fResponseObjectBuffer.Clear();
end;

procedure TJSONRPCClient.ConnectionDataReadyToReceive(ASocket: TLSocket);
var
  lStrUTF8: UTF8String;
  lStrUnicode, e: UnicodeString;
  l: Integer;
  lCharsRead: LongWord;
  p: Int64;
  lResponse: TJSONRPCResponse;
  lExitLoop: Boolean;

begin
  // JSON is UTF-8 encoded, thus we call GetMessage() with a variable of type UTF8String
  l := ASocket.GetMessage(lStrUTF8);
  if l > 0 then begin
    p := fResponseStream.Position;
    try
      fResponseStream.Position := fResponseStream.Size;
      fResponseStream.WriteString(lStrUTF8);
    finally
      fResponseStream.Position := p;
    end;

    // create response objects from data stream
    repeat
      lExitLoop := true;

      lStrUTF8 := fResponseStream.ReadString(fResponseStream.Size - p);
      l := Length(lStrUTF8);
      if l > 0 then begin
        // convert UTF-8 string to UnicodeString manually as FPC does not do
        // it automatically when passed into Read()
        lStrUnicode := UTF8Decode(lStrUTF8);

        lResponse := TJSONRPCResponse.Create();
        if lResponse.Read(lStrUnicode, e, lCharsRead) then begin
          // response object successfully created from data stream part

          // remove that part from data stream
          lStrUTF8 := UTF8Copy(lStrUTF8, p + lCharsRead + 1, Length(lStrUTF8));

          fResponseStream.Size := 0;
          fResponseStream.WriteString(lStrUTF8);

          p := 0;

          // remember object in buffer
          fResponseObjectBuffer.Add(lResponse);

          // try to create another object in next loop
          lExitLoop := false;
        end else
          // data stream most likely not fully received, thus discard instance
          lResponse.Free;
      end;

      fResponseStream.Position := p;

    until lExitLoop;
  end;
end;

function TJSONRPCClient.Connect(const AHost: UnicodeString; const APort: LongWord): Boolean;
begin
  if not fTCPClient.Connected then
    result :=  fTCPClient.Connect(AHost, APort)
  else
    result := true;
end;

procedure TJSONRPCClient.Disconnect();
begin
  fTCPClient.Disconnect();
end;

function TJSONRPCClient.SendRequest(const ARequest: TJSONRPCRequest;
                                    const AResponseTimeout: Integer;
                                    out AResponse: TJSONRPCResponse;
                                    var AErrorMessage: UnicodeString): Boolean;
var
  lQuery: UTF8String;
  t: LongWord;
  lEventWaitForResult: TWaitResult;
  lResponse: TJSONRPCResponse;

begin
  result := false;
  AErrorMessage := '';
  AResponse := nil;

  if fTCPClient.Connected then begin

    if Assigned(ARequest) then begin

      ARequest.Version := '2.0';
      ARequest.ID := IntToStr(GetTickCount());

      // call to UTF8Encode() as JSON needs to be UTF-8 encoded
      lQuery := UTF8Encode(ARequest.ToString());
      if lQuery <> '' then begin

        fTCPClient.SendMessage(lQuery);

        ProcessEvents();

        t := GetTickCount();
        repeat
          if fResponseObjectBuffer.Find(ARequest.ID, lResponse) then begin
            AResponse := fResponseObjectBuffer.Extract(lResponse);

            break;
          end;

          lEventWaitForResult := fCancelSendRequestEvent.WaitFor(1);

          if lEventWaitForResult <> wrSignaled then
            ProcessEvents();

        until (lEventWaitForResult = wrSignaled) or ((GetTickCount() - t) > AResponseTimeout);

        if Assigned(AResponse) then
          result := true
        else
          AErrorMessage := 'No response';

      end else
        AErrorMessage := 'Request is empty';

    end else
      AErrorMessage := 'No request instance passed';

  end else
    AErrorMessage := 'Not connected to XBMC';
end;

function TJSONRPCClient.GetConnected(): Boolean;
begin
  fTCPEventer.Timeout := 0;
  fTCPEventer.CallAction();

  result := fTCPClient.Connected;
end;

function TJSONRPCClient.GetHost(): UnicodeString;
begin
  result := fTCPClient.Host;
end;

function TJSONRPCClient.GetPort(): LongWord;
begin
  result := fTCPClient.Port;
end;

function TJSONRPCClient.GetResponseObjectBufferSize(): Integer;
begin
  result := fResponseObjectBuffer.Count;
end;

{$ENDREGION}

end.
